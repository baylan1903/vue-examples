<?php
header("Content-type: application/json");
const newPassage = [
"title" => "Catch-22",
"subtitle" => "Joseph Heller",
"passage" => "Like Olympic medals and tennis trophies, all they signified was that the owner had done something of no benefit to anyone more capably than everyone else."
];
echo json_encode(newPassage);
?>