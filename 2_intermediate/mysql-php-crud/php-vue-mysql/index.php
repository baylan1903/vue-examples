<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Vue + PHP + MySQL</title>
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.5/css/bulma.min.css" />
      <link rel="stylesheet" href="assets/css/general.css" />
      <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
  </head>
  <body>

    <section class="section">
      <div class="container">
      <div id="app" v-cloak>
        <h1 class="title">Contact List <a class="button is-link is-outlined has-addons is-right" @click="isActive = !isActive">Add a new contact</a></h1>
          <table class="table is-bordered is-hoverable is-fullwidth">
            <thead><tr><th v-for="field in fields" v-text="field"></th></tr></thead>
            <tbody>
              <tr v-if="!contacts.length"><td colspan="8">Veriler getiriliyor...</td></tr>
              <tr v-for="(contact, index) in contacts" :key="index" v-else>
                <th>{{ index+1 }}</th>
                <td>{{ contact.id }}</td>
                <td>{{ contact.name }}</td>
                <td>{{ contact.email }}</td>
                <td>{{ contact.country }}</td>
                <td>{{ contact.city }}</td>
                <td>{{ contact.job }}</td>
                <td class="field has-addons">
                    <p class="control"><a class="button is-link is-info is-outlined has-addons is-right" @click="isActive = !isActive; updateContact(index, contact.id);">Edit</a></p>
                    <p class="control"><a class="button is-link is-danger is-outlined has-addons is-right" @click="deleteUser(contact.id)">Delete</a></p>
                </td>
              </tr>
            </tbody>
            <tfoot><tr><th v-for="field in fields" v-text="field"></th></tr></tfoot>
          </table>

          <div class="modal" :class="{'is-active': isActive}">
            <div class="modal-background"></div>
            <div class="modal-card">
              <header class="modal-card-head">
                <p class="modal-card-title" v-if="contact.id !== 0">Edit Contact</p>
                <p class="modal-card-title" v-else>New Contact</p>
                <button class="delete" aria-label="close" @click="isActive = !isActive"></button>
              </header>
              <section class="modal-card-body">
                  <div class="field">
                    <label class="label">{{ this.fields[2] }}</label>
                    <div class="control">
                      <input class="input" type="text" placeholder="Your name" v-model="contact.name" />
                    </div>
                  </div>
                  <div class="field">
                    <label class="label">{{ this.fields[3] }}</label>
                    <div class="control">
                      <input class="input" type="text" placeholder="@domain.com" v-model="contact.email" />
                    </div>
                  </div>
                  <div class="field">
                    <label class="label">{{ this.fields[4] }}</label>
                    <div class="control">
                      <input class="input" type="text" placeholder="" v-model="contact.country" />
                    </div>
                  </div>
                  <div class="field">
                    <label class="label">{{ this.fields[5] }}</label>
                    <div class="control">
                      <input class="input" type="text" placeholder="" v-model="contact.city" />
                    </div>
                  </div>
                  <div class="field">
                    <label class="label">{{ this.fields[6] }}</label>
                    <div class="control">
                      <div class="select is-fullwidth">
                        <select name="job" v-model="contact.job">
                          <option value="" disabled>Select dropdown</option>
                          <option value="UX Designer">UX Designer</option>
                          <option value="Graphic Designer">Graphic Designer</option>
                          <option value="Web Designer">Web Designer</option>
                          <option value="Mobile App Designer">Mobile App Designer</option>
                          <option value="Front-end Developer">Front-end Developer</option>
                          <option value="Back-end Developer">Back-end Developer</option>
                        </select>
                      </div>
                    </div>
                  </div>
              </section>
              <footer class="modal-card-foot">
                <button v-if="contact.id !== 0" class="button is-success" @click="saveContact(contact.id)">Save contact</button>
                <button v-else class="button is-success" @click="createContact()">Add contact</button>
                <button v-if="contact.id == 0" class="button" @click="resetForm()">Cancel</button>
              </footer>
            </div>
            <button class="modal-close is-large" aria-label="close" @click="isActive = !isActive"></button>
          </div>
        </div>
      </div>
    </section>

	  <script src="https://cdn.jsdelivr.net/npm/vue"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script src="assets/js/app.js"></script>
  </body>
</html>
