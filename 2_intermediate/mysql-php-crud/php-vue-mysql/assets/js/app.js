var app = new Vue({
  el: '#app',
  data() {
    return {
      isActive: false,
      contact: {
        id: '',
        name: '',
        email: '',
        country: '',
        city: '',
        job: ''
      },
      contacts: [],
      currentContact: [],
      fields: ['Position', 'ID', 'Name', 'E-mail', 'Country', 'City', 'Job', 'Manage'],
    }
  },

  mounted: function () {
    this.getContacts();
  },

  methods: {

    getContacts: function () {
      axios.get('api/api.php')
        .then(function (response) {
          console.log(response.data);
          app.contacts = response.data;
        })
        .catch(function (error) {
          console.log(error);
        });
    },

    createContact: function () {
      let formData = new FormData();

      formData.append('name', this.contact.name)
      formData.append('email', this.contact.email)
      formData.append('city', this.contact.city)
      formData.append('country', this.contact.country)
      formData.append('job', this.contact.job)

      var contact = {};
      formData.forEach(function (value, key) {
        contact[key] = value;
        console.log(contact[key]);
      });

      axios({
          method: 'post',
          url: 'api/api.php',
          data: formData,
          config: {
            headers: {
              'Content-Type': 'multipart/form-data'
            }
          }
        })
        .then(function (response) {
          console.log(response)
          app.contacts.push(app.contact)
          app.resetForm();
          app.getContacts();
        })
        .catch(function (response) {
          console.log(response)
        });

    },
    saveContact: function (contactId) {

      let formData = new FormData();

      formData.append('id', contactId)
      formData.append('name', this.contact.name)
      formData.append('email', this.contact.email)
      formData.append('city', this.contact.city)
      formData.append('country', this.contact.country)
      formData.append('job', this.contact.job)

      var contact = {};
      console.log(formData);
      formData.forEach(function (value, key) {
        contact[key] = value;
        console.log(contact[key]);
      });
      axios({
          method: 'post',
          url: 'api/api.php?action=update&id=' + contactId,
          data: formData,
          config: {
            headers: {
              'Content-Type': 'multipart/form-data'
            }
          }
        })
        .then(function (response) {
          console.log(response);
          app.contacts.push(app.contact)
          app.resetForm();
          app.getContacts();
        })
        .catch(function (response) {
          console.log(response)
        });

    },
    updateContact: function (index, contactId) {
      if (app.contacts[index].id == contactId) {
        Vue.set(app.contact, 'id', contactId)
        Vue.set(app.contact, 'name', app.contacts[index].name)
        Vue.set(app.contact, 'email', app.contacts[index].email)
        Vue.set(app.contact, 'country', app.contacts[index].country)
        Vue.set(app.contact, 'city', app.contacts[index].city)
        Vue.set(app.contact, 'job', app.contacts[index].job)
      }
    },

    deleteUser: function (contactId) {
      axios.get('api/api.php?action=delete&id=' + contactId)
        .then(function (response) {
          console.log(response);
          app.getContacts();
        })
        .catch(function (response) {
          console.log(response)
        });
    },

    resetForm: function () {
      this.contact.name = '';
      this.contact.email = '';
      this.contact.country = '';
      this.contact.city = '';
      this.contact.job = '';
      console.log('Cleaned!')
    }
  },
});