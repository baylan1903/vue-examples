import singleFileComponent from './comp.mjs';

var app = new Vue({
  el: '#app',
  data: {
    message: 'Hello Vue 2.6.0-beta1 ESM Browser Build!',
    isLoading: false
  },
  components: {
    singleFileComponent
  },
  computed: {

  },
  methods: {
    loading() {
      (this.isLoading) ? this.isLoading = false: this.isLoading = true
    }
  },
  created() {
    this.loading()
    console.log('loading!')
  },
  mounted() {
    this.loading()
    console.log('loaded')
  }
});