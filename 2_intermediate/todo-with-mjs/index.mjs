import App from './App.mjs'

new Vue({
  el: '#app',
  template: '<App/>',
  components: {
    App
  }
})